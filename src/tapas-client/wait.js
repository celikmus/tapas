define(["dojo/Deferred"], function(Deferred){
	var wait = function(milliseconds){
		if(typeof(milliseconds) === 'undefined')
			return undefined;
		else
			return new Deferred();
	};

	return wait;


});
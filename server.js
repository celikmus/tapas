// Initial Module to Load
var initModule = "tapas-server/main";

// Dojo Configuration
dojoConfig = {
	baseUrl: "src/",
	async: 1,

	hasCache: {
		"host-node": 1,
		"dom": 0
	},

	packages: [{
		name: "dojo",
		location: "dojo"
	},{
		name: "dijit",
		location: "dijit"
	},{
		name: "dojox",
		location: "dojox"
	},{		
		name: "tapas-server",
		location: "tapas-server"
	}],

	deps: [initModule]
};

// Load dojo/dojo
require("./src/dojo/dojo.js");
define([
	'dojo/node!express',
	'dojo/node!jade',
	'dojo/node!stylus',
	'dojo/node!nib',
	'dojo/node!colors',
	'dojo/node!gm',
	'dojo/Deferred',
	'dojo/promise/all',
	'dojo/node!fs',
	'dojo/node!http',
	'dojo/node!http-proxy'
], function(express, jade, stylus, nib, colors, gm, Deferred, all, dfs, http, http_proxy){

	/* Setup Express Server */
	var app = express(),
		appPort = process.env.PORT || 8001,
		env = process.env.NODE_ENV || 'development',
		root = '/lib';

	function compile(str, path){
		return stylus(str).
			set('filename', path).
			set('compress', true).
			use(nib());
	}

	app.configure(function(){
		app.locals.pretty = true;
		app.set('view engine', 'jade');
		app.set('views', 'views');
		app.use(express.compress());
		app.use(express.logger(env === 'production' ? null : 'dev'));
		// app.use(express.cookieParser());
		// app.use(express.cookieSession({ secret: 'yHCoyEPZ9WsNDORGb9SDDMNn0OOMcCgQiW5q8VFhDHJiztvvVVCPkZQWUAXl' }));
		// app.use(express.favicon('./images/favicon.ico'));

		app.use(stylus.middleware({
			src: '.',
			compile: compile,
			compress: true
		}));

		app.use('/src', express['static']('./src'));
		app.use('/lib', express['static']('./lib', { maxAge: 86400000 }));
		app.use('/css', express['static']('./css', { maxAge: 86400000 }));
		app.use('/images', express['static']('./images', { maxAge: 86400000 }));

		app.use(app.router);

		app.use('/500', function(request, response, next){
			next(new Error('All your base are belong to us!'));
		});

		app.use(function(request, response, next){
			if(request.accepts('html')){
				response.status(404);
				response.render('404', {
					url: request.url,
					root: root
				});
				return;
			}

			if(request.accepts('json')){
				response.send({ error: 'Not Found' });
				return;
			}

			response.type('txt').send('Not Found');
		});

		app.use(function(error, request, response, next){
			response.status(error.status || 500);
			response.render('500', {
				error: error,
				root: root
			});
		});
	});

	app.get('/', function(request, response, next){
		response.render('index', {
			root: root
		});
		console.log("rendering root");
	});


	app.get('/page/:page', function(request, response, next){
		var d = new Deferred();
		jade.renderFile('views/content/' + request.params.page + '.jade', {}, function(err, page){
			if(err){
				d.reject(err);
			}else{
				d.resolve(page);
			}
		});
		// if(request.params.page == 'cv'){
		// 	var d2 = new Deferred();
		// 	jade.renderFile('views/content/cv_nav.jade', {}, function(err, page){
		// 		if(err){
		// 			d2.reject(err);
		// 		}else{
		// 			d2.resolve(page);
		// 		}
		// 	});
		// 	d = all([d, d2]);
		// }
		d.then(function(page){
			var result = {
				title: 'Kitson P. Kelly'
			};
			if(page instanceof Array){
				result.content = page[0];
				result.nav = page[1];
			}else{
				result.content = page;
			}
			switch(request.params.page){
				case 'home':
					result.connectItems = true;
					result.subtitle = 'presence on the interwebs';
					break;
				case 'dojo':
					result.connectItems = false;
					result.subtitle = 'dojo toolkit';
					break;
			}
			response.json(result);
		}, function(err){
			next(err);
		});
	});

	app.get('/views/:view', function(request, response, next){
		response.render(request.params.view, {
			root: root,
			url: '/foo',
			error: new Error('All your base are belong to us!')
		});
	});

	app.get('/data/', function(request, response, next){

			console.log("data call");
			var data = dfs.readFileSync("data1.json");

   			response.render(data, {
   				root: root,
   				error: new Error('Cant read tapas file')
   			});

	});

	app.get('/404', function(request, response, next){
		next();
	});

	app.get('/403', function(request, response, next){
		var error = new Error('not allowed!');
		error.status = 403;
		next(error);
	});

	app.get('/500', function(request, response, next){
		// TODO: Handle different response encodings (html/json)
		next(new Error('All your base are belong to us!'));
	});



	// var myproxy = http_proxy.createServer(8001, 'localhost');
	// app.use('/data', myproxy);
	http_proxy.createServer(8900, 'localhost').listen(9000);

	http.createServer(function (req, res) {
  		res.writeHead(200, { 'Content-Type': 'text/plain' });
  		// res.write('request successfully proxied!' + '\n' + JSON.stringify(req.headers, true, 2));

			var data = dfs.readFileSync("data1.json", "utf8");
			var obj = JSON.parse(data);
			console.log(obj.message);
			res.write(obj.message);
   			
  		res.end();
	}).listen(8900);

	// var myData = dfs.readFileSync("src/tapas-server/data.json");
	// console.log(JSON.parse(myData));

	app.listen(appPort);



	console.log('HTTP server started on port '.grey + appPort.toString().cyan);
});
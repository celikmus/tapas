var profile = (function () {
	return {
		releaseDir: '../lib',
		basePath: 'src',
		action: 'release',
		mini: true,
		layerOptimize: 'closure',
		cssOptimize: 'comments',
		selectorEngine: 'lite',

		packages: [{
			name: 'dojo',
			location: 'dojo'
		},{
			name: 'dijit',
			location: 'dijit'
		},{
			name: 'dojox',
			location: 'dojox'
		},{			
			name: 'tapas-client',
			location: 'tapas-client'
		}],

		defaultConfig: {
			hasCache: {
				'dojo-built': 1,
				'dojo-loader': 1,
				'dom': 1,
				'host-browser': 1,
				'config-selectorEngine': 'lite'
			},
			async: 1
		},

		staticHasFeatures: {
		},

		layers: {
			'dojo/dojo': {
				include: [ 'dojo/dojo', 'tapas-client/main'],
				customBase: 1,
				boot: 1
			}
		}
	};
})();
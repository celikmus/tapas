define([
    'intern!object',
    'intern/chai!assert',
    'tapas-client/wait',
    'node_modules/intern/node_modules/dojo/request'
], function (registerSuite, assert, wait, request) {
    registerSuite({
        name: 'wait',

        'without any paramters': function () {
            assert.typeOf(wait(), 'undefined', 'wait with no arguments should return undefined');
        },
        'number passed as parameter': function () {
            assert.typeOf(wait(2000), 'object', 'wait with number input should return object');
        },
        'async test': function () {
            var dfd = this.async(1000);

            request('/data').then(dfd.callback(function (data) {
               assert.strictEqual(data, 'hello worold');
            }, dfd.reject.bind(dfd)));
        }

    });
});